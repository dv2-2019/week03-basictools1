function showAlert(text) {
    console.log(text)
}
function listFormElement() {
    var formElements = document.getElementsByTagName('form');
    console.log(formElements)
}

function copyTextFromNameToLastName() {
    var nameElement = document.getElementById('firstName')
    var surnameElement = document.getElementById('lastName')
    var firstName = nameElement.value

    console.log('first name is ' + firstName)
    console.log('now lastname is ' + surnameElement.value)
    surnameElement.value = firstName

    console.log('then lastname is ' + firstName)
}

function resetForm() {
    document.getElementById('firstName').value = '';
    document.getElementById('lastName').value = '';
    document.getElementById('email').value = '';
    document.getElementById('password1').value = '';
    document.getElementById('password2').value = '';
    console.log('the form already reset')
}

function getRadioValue(inlineRadioOptions) {
    document.getElementById('email').value = inlineRadioOptions
    console.log('radio value is ' + inlineRadioOptions)
}

function showSummary() {
    var name = document.getElementById('firstName').value
    var lastname = document.getElementById('lastName').value
    var email = document.getElementById('email').value
    var password = document.getElementById('password1').value
    var confpassword = document.getElementById('password2').value

    var summaryElement = document.getElementById('summary')

    summaryElement.innerHTML = "First Name : " + name + "<br> Last Name : "
        + lastname + " <br> Email : " + email + "<br> Password :  " + password + "<br> Confirm Password : " + confpassword

    if (name == "" && lastname == "" && email == "" && password == "" && confpassword == "") {
        summaryElement.setAttribute('hidden', 'true');
    } else {
        summaryElement.removeAttribute('hidden')
    }
}


function addElement() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
    console.log(placeHolder)
}

function addHref() {
    var node = document.createElement('a')
    node.innerHTML = 'google'
    node.setAttribute('href', 'http://www.google.com')
    node.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
}
function addBoth() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var node2 = document.createElement('p')
    node2.setAttribute('href', 'https://www.google.com')
    node2.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
    placeHolder.appendChild(node2)
}

function addForm() {

    var text = document.createElement('h')
    text.innerHTML = 'please fill in this form to create account'
    text.setAttribute('class', 'lead')
    var placeHolder = document.getElementById('header')
    placeHolder.appendChild(text)
    console.log(placeHolder)

    var node = document.createElement('input')
    node.setAttribute('type', 'text')
    node.setAttribute('class', 'form-control')
    node.setAttribute('placeholder', 'First Name')
    console.log(node)
    var placeholder = document.getElementById('fn')
    placeholder.appendChild(node)

    var node2 = document.createElement('input')
    node2.setAttribute('type', 'text')
    node2.setAttribute('class', 'form-control')
    node2.setAttribute('placeholder', 'Last Name')
    console.log(node2)
    var placeholder = document.getElementById('ln')
    placeholder.appendChild(node2)

    var node3 = document.createElement('input')
    node3.setAttribute('type', 'email')
    node3.setAttribute('class', 'form-control')
    node3.setAttribute('placeholder', 'Email')
    console.log(node3)
    var placeholder = document.getElementById('m')
    placeholder.appendChild(node3)

}


function addTable() {
    //create table thead
    var table = document.createElement('table');
    table.setAttribute('class', 'table table-striped');

    var arrHead = new Array();
    arrHead = ['#', 'First', 'Last', 'Handle'];

    var thead = document.createElement('thead')
    table.appendChild(thead)

    var tr = document.createElement('tr')
    thead.appendChild(tr)

    for (var h = 0; h < arrHead.length; h++) {
        var th = document.createElement('th');
        th.innerHTML = arrHead[h];
        tr.appendChild(th);
    }

    //create table tbody
    var arrValue = new Array();
    arrValue.push(['1', 'Mark', 'Otto', '@mdo']);
    arrValue.push(['2', 'Jacob', 'Thronton', '@fat']);
    arrValue.push(['3', 'Larry', 'The bird', '@twitter']);

    var tbody = document.createElement('tbody')
    table.appendChild(tbody)
    for (var c = 0; c <= arrValue.length - 1; c++) {
        tr = table.insertRow(-1);

        for (var j = 0; j < arrHead.length; j++) {
            var th = document.createElement('th');
            th.setAttribute('scope', 'row')
            tr.appendChild(th)
            th.innerHTML = arrValue[c][j];

            for (var d = 1; d < arrHead.length; d++) {
                var td = document.createElement('td')
                tr.appendChild(td)
                td.innerHTML = arrValue[c][d]
            } break
        }

        tbody.appendChild(tr)
    }
    var placeholder = document.getElementById('table')
    placeholder.appendChild(table)
}

function countNumber() {
    //head
    var table = document.createElement('table')
    table.setAttribute('class', 'table table-bordered ')

    var thead = document.createElement('thead')
    table.appendChild(thead)

    var tr = document.createElement('tr')
    thead.appendChild(tr)

    var th = document.createElement('th');
    th.innerHTML = '#';
    tr.appendChild(th);

    var placeholder = document.getElementById('num')
    placeholder.appendChild(table)

    //row number
    for (var x = 1; x < 5; x++) {
        var tr = document.createElement('tr')
        table.appendChild(tr)

        var td = document.createElement('td')
        td.innerHTML = x
        tr.appendChild(td)

        var placeholder = document.getElementById('num')
        placeholder.appendChild(table)
    }

}